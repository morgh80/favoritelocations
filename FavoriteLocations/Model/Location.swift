//
//  Location.swift
//  FavoriteLocations
//
//  Created by aeronaut on 04.01.2018.
//  Copyright © 2018 aeronaut. All rights reserved.
//

import Foundation
import UIKit

class Location {
    private(set) var name: String
    private(set) var adress: String
    private(set) var description: String
    private(set) var image: UIImage
    
    init (name: String, adress: String, description: String, image: UIImage) {
        self.name = name
        self.adress = adress
        self.description = description
        self.image = image
    }
    
}
