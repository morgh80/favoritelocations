//
//  NewLocationController.swift
//  FavoriteLocations
//
//  Created by aeronaut on 06.01.2018.
//  Copyright © 2018 aeronaut. All rights reserved.
//

import UIKit
import CoreData

class NewLocationController: UITableViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var savedLocation: Location?
    var location: LocationMO!
    
    var nameToEdit: String?
    var adressToEdit: String?
    var descrToEdit: String?
    var imageToEdit: Data?
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var addPhotoLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField! {
        didSet {
            nameTextField.tag = 1
            nameTextField.becomeFirstResponder()
            nameTextField.delegate = self
        }
    }
    
    @IBOutlet weak var adressTextField: UITextField! {
        didSet {
            adressTextField.tag = 2
            adressTextField.delegate = self
        }
    }
    
    @IBOutlet weak var descriptonTextView: UITextView! {
        didSet {
            descriptonTextView.tag = 3
            descriptonTextView.text = ""
        }
    }
    
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        if nameTextField.text == "" || adressTextField.text == "" || descriptonTextView.text == "" || !addPhotoLabel.isHidden {
            let alertController = UIAlertController(title: "Alert", message: "Please fill all fields or add photo", preferredStyle: .alert)
            let cancel = UIAlertAction(title: "OK", style: .cancel, handler: { (action) -> Void in })
            alertController.addAction(cancel)
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            if let appDelegate = (UIApplication.shared.delegate as? AppDelegate) {
                location = LocationMO(context: appDelegate.persistentContainer.viewContext)
                location.name = nameTextField.text
                location.adress = adressTextField.text
                location.descr = descriptonTextView.text
                if let image = photoImageView.image {
                    location.image = UIImagePNGRepresentation(image)
                }
                print("saving...")
                appDelegate.saveContext()
                dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let name = nameToEdit {
            if let adress = adressToEdit {
                if let descr = descrToEdit {
                    if let image = imageToEdit {
                    nameTextField.text = name
                    adressTextField.text = adress
                    descriptonTextView.text = descr
                        photoImageView.image = UIImage(data: image as Data)
                        addPhotoLabel.text = "ADD NEW PHOTO"
                }
                }
            }
        }
        // Do any additional setup after loading the view.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextTextField = view.viewWithTag(textField.tag + 1) {
            textField.resignFirstResponder()
            nextTextField.becomeFirstResponder()
        }
        return true
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            
            let photoSourceRequestController = UIAlertController(title: "", message: "Choose your photo source", preferredStyle: .actionSheet)
            
            let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: { (action) in
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                    imagePicker.sourceType = .camera
                    
                    self.present(imagePicker, animated: true, completion: nil)
                    self.addPhotoLabel.isHidden = true
                }
            })
            
            let photoLibraryAction = UIAlertAction(title: "Photo library", style: .default, handler: { (action) in
                if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                    imagePicker.sourceType = .photoLibrary
                    
                    self.present(imagePicker, animated: true, completion: nil)
                    self.addPhotoLabel.isHidden = true
                }
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            photoSourceRequestController.addAction(cameraAction)
            photoSourceRequestController.addAction(photoLibraryAction)
            photoSourceRequestController.addAction(cancelAction)
            
            present(photoSourceRequestController, animated: true, completion: nil)
            
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            photoImageView.image = selectedImage
        }
        dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
}
