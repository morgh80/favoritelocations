//
//  LocationDetailViewController.swift
//  FavoriteLocations
//
//  Created by aeronaut on 04.01.2018.
//  Copyright © 2018 aeronaut. All rights reserved.
//

import UIKit

class LocationDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var location: LocationMO!
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var headerView: LocationDetailView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        navigationItem.largeTitleDisplayMode = .never
        
        // Configure header view
        headerView.nameLabel.text = location.name
        tableView.separatorStyle = .none
        
        if let locationImage = location.image {
            headerView.headerImageView.image = UIImage(data: locationImage as Data)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Adress", for: indexPath) as! LocationDetailAdressCell
            cell.adress.text = location.adress
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Description", for: indexPath) as! LocationDetailDescriptionCell
            cell.descriptionLabel.text = location.descr
            return cell
            
        default:
            fatalError("Failed to initiate cell")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editLocation" {
            let nav = segue.destination as! UINavigationController
            let destinationController = nav.topViewController as! NewLocationController
            destinationController.location = location
            destinationController.nameToEdit = location.name
            destinationController.adressToEdit = location.adress
            destinationController.descrToEdit = location.descr
            destinationController.imageToEdit = location.image
        }
    }
    
}
