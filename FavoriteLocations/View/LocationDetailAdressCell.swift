//
//  LocationDetailAdressCell.swift
//  FavoriteLocations
//
//  Created by aeronaut on 05.01.2018.
//  Copyright © 2018 aeronaut. All rights reserved.
//

import UIKit

class LocationDetailAdressCell: UITableViewCell {

    @IBOutlet weak var adress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
