//
//  LocationsListTableViewCell.swift
//  FavoriteLocations
//
//  Created by aeronaut on 04.01.2018.
//  Copyright © 2018 aeronaut. All rights reserved.
//

import UIKit

class LocationsListTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var adress: UILabel!
    @IBOutlet weak var locationImage: UIImageView! {
        didSet {
            locationImage.layer.cornerRadius = 30
            locationImage.clipsToBounds = true
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
