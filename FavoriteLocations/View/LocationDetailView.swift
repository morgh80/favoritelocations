//
//  LocationDetailView.swift
//  FavoriteLocations
//
//  Created by aeronaut on 05.01.2018.
//  Copyright © 2018 aeronaut. All rights reserved.
//

import UIKit

class LocationDetailView: UIView {

    @IBOutlet var headerImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
